const express = require("express")
const app = express()
const port = process.env.PORT || 4000

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// controller
const book = require("./controllers/bookController")
const author = require("./controllers/authorController")

app.get("/", (req, res) => {
  res.json({ status: "Auto Deployment!" })
})

app.get("/books", book.getAll)
app.post("/books", book.create)

app.get("/authors", author.getAll)
app.post("/authors", author.create)

app.listen(port, () => console.log(`Server ready on ${port}`))