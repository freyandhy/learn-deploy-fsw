const { Author } = require("../models")

module.exports = {
  getAll: (req, res) => {
    Author.findAll()
      .then(result => {
        res.status(200).json({
          status: "success",
          data: result
        })
      })
  },
  create: (req, res) => {
    Author.create({
      name: req.body.name,
      gender: req.body.gender
    })
    .then(() => {
      res.status(201).json({
        status: "success",
        message: "Create Author success!"
      })
    })
  }
}