const { Book } = require("../models")

module.exports = {
  getAll: (req, res) => {
    Book.findAll({
      include: "Author"
    })
    .then(result => {
      res.status(200).json({
        status: "success",
        data: result
      })
    })
  },
  create: (req, res) => {
    const { title, genre, authorId } = req.body
    Book.create({
      title: title,
      genre: genre,
      authorId: authorId
    })
    .then(() => {
      res.status(201).json({
        status: "success",
        message: "Create Book success!"
      })
    })
  }
}